//
//  Coding_Test_TCS_021622App.swift
//  Coding-Test-TCS-021622
//
//  Created by Ajay on 2/16/22.
//

import SwiftUI

@main
struct Coding_Test_TCS_021622App: App {
    var body: some Scene {
        WindowGroup {
            LoginView(user: User(userName: "AJ", address: "NJ", role: "Developer"))
        }
    }
}

//
//  LoginViewModel.swift
//  Coding-Test-TCS-021622
//
//  Created by Ajay on 2/16/22.
//

import Foundation
import SwiftUI

struct Constants{
    struct URL{
        static let iTuneList = "https://itunes.apple.com/search"
    }
    
    struct QueryString{
        static let iTuneListQueryString = "media=music&entity=song&term=muse"
    }
}

class LoginViewModel: ObservableObject {
    @Published var userName: String = ""
    @Published var password: String = ""
    
    
    func login(success: @escaping (_ response: User) -> Void, failure: @escaping (_ errorDescription: String) -> Void) {
        print("user Name: \(userName)")
        getITunesList()
        
        
        // MARK: - Sample API call intergration
        func getITunesList(){
            let urlConfiguration = URLSessionConfiguration.default
            urlConfiguration.requestCachePolicy = .reloadIgnoringLocalCacheData
            urlConfiguration.urlCache = nil
            let urlSession = URLSession(configuration: urlConfiguration)
            if var urlComponenet = URLComponents(string: Constants.URL.iTuneList) {
                urlComponenet.query = Constants.QueryString.iTuneListQueryString
                
                guard let url = urlComponenet.url else {
                    return
                }
                
                print("URL is \(url)")
                let dataTask = urlSession.dataTask(with: url) { (data, response, error) -> () in
                    guard let responseData = response as? HTTPURLResponse else {
                        return
                    }
                    if responseData.statusCode == 200 {
                        // Serialize the data here
                        //JSONSerialization.jsonObject(with: data, options: []) as [String: Any]
                        
                        // Success CallBack incase of Success
                        success(User(userName: self.userName, address: "NJ", role: "Developer"))
                    } else {
                        if let error = error {
                            // Failure callback incase of known error
                            failure(error.localizedDescription)
                        } else {
                            // Failure callback incase of unknow error
                            failure("Something went wrong. Please try again.")

                        }
                    }
                }
                    dataTask.resume()
            }
        }
    }
}


struct User{
    var userName: String
    var address: String
    var role: String
}

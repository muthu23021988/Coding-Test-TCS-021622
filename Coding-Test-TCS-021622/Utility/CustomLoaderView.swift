//
//  CustomLoaderView.swift
//  Coding-Test-TCS-021622
//
//  Created by Ajay on 2/18/22.
//

import SwiftUI

struct CustomLoaderView: View {
   
    @Binding var isAnimating: Bool
    var body: some View {
        VStack {
            if self.isAnimating {
                GeometryReader{geometry in
                    Loader()
                    // Center the loader.
                        .position(x: geometry.frame(in: .local).midX, y: geometry.frame(in: .local).midY)
                                    }.background(Color.black.opacity(0.45).edgesIgnoringSafeArea(.all))
            }
        }
        .onAppear(perform: {
        })
    }
    
}

struct Loader: View {
    @State var animate = false
    var body: some View {
        VStack {
        Circle()
            .trim(from: 0, to: 0.8)
            .stroke(AngularGradient(gradient: .init(colors: [.red, .black]), center: .center), style: StrokeStyle(lineWidth: 8, lineCap: .round))
            .frame(width: 45, height: 45)
            .rotationEffect(.init(degrees: self.animate ? 360 : 0))
            .animation(Animation.linear.repeatForever(autoreverses: false), value: self.animate)
        }
        .onAppear {
            self.animate.toggle()
        }
    }

}

struct CustomLoaderView_Previews: PreviewProvider {
    static var previews: some View {
        CustomLoaderView(isAnimating: .constant(true))
    }
}

//
//  LoginView.swift
//  Coding-Test-TCS-021622
//
//  Created by Ajay on 2/16/22.
//

import SwiftUI

struct LoginView: View {
    @State private var isAlertPresented = false
    @State private var isLoginFailed = false
    @ObservedObject var observable = LoginViewModel()
    @State var isDetailViewActive = false
    @State var isNavigationBarHidden = false
    @State var user: User
    @State var isAnimating = false
    @FocusState var isPasswordFocus: Bool
    var body: some View {
        NavigationView {
            ZStack {
                VStack (alignment: .center, spacing: 15, content: {
                    Spacer()
                    VStack (alignment: .leading, spacing: 5, content: {
                        Text("User Name:")
                            .font(.caption)
                            .padding()
                            .foregroundColor(.gray)
                    TextField("Enter User Name", text:
                                $observable.userName)
                            .foregroundColor(.black)
                            .focused($isPasswordFocus)
                    .padding()
  
                        Text("Password:")
                            .padding()
                            .font(.caption)
                            .foregroundColor(.gray)
                    SecureField("Password", text: $observable.password, prompt: Text("Enter Password"))
                            .foregroundColor(.black)
                            .focused($isPasswordFocus)
                            .padding()
                        
                    })
                        .border(Color.gray, width: 2)
                    .padding()
                    Spacer(minLength: 10)
                    
                    Button(action: {
                        self.isPasswordFocus = false
                        self.isAnimating = true
                        // Call API if text entered.
                        if !self.observable.userName.isEmpty && !self.observable.password.isEmpty {
                        observable.login(success: { (response) in
                            // Success Callback
                            print("Response - \(response)")
                            self.isAnimating.toggle()
                            self.isDetailViewActive = true
//                            self.isPushed = true
                            self.user = response
                        }, failure: { (error) in
                            // Failure callback
                            self.isAnimating.toggle()
                            self.isLoginFailed = true
                            print("Error\(error)")
                        })
                        } else {
                            self.isAlertPresented.toggle()
                        }
                    }, label: {
                        Text("Login")
                            .frame(width: 150, height: 30)
                    })
                    .frame(width: 150, height: 30)
                    .foregroundColor(.white)
                    .background(Color.gray)
                    .cornerRadius(5)
                    .contentShape(Rectangle())
                    .alert(isPresented: $isAlertPresented, content: {
                        Alert(title: Text("Alert"), message: Text(self.observable.userName.isEmpty ? "Please enter user Name" :  "Please enter password"), dismissButton:   .default(Text("OKay")){
                            
                        })
                    })
                    // Bind data to detail view.
                    NavigationLink(destination: LandingDetailView(isNavigationBarHidden: self.$isNavigationBarHidden, message: self.$observable.userName, isPushed: self.$isDetailViewActive, user: self.$user), isActive: $isDetailViewActive){
                       EmptyView()
                    }
                    Spacer()
                    
                })
                    .padding()
                CustomLoaderView(isAnimating: $isAnimating)
            }
        }
        .navigationBarHidden(self.isNavigationBarHidden)
        .onAppear() {
            self.isNavigationBarHidden = true
        }
    }
}

struct PrimaryButton: View {
    @ObservedObject var observer = LoginViewModel()
//    @Binding var message: String
    var body: some View {
        Button {
            print("Button Pressed")
            observer.login(success: { (response) in
                print("Success")
            }) { error in
                print("Error \(error)")
            }
        } label: {
            Text("Login")
        }
        .frame(width: 150, height: 30)
        .foregroundColor(.white)
        .background(Color.gray)
        .cornerRadius(5)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView(user: User(userName: "John", address: "NJ", role: "Developer"))
    }
}



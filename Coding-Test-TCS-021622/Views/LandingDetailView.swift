//
//  LandingDetailView.swift
//  Coding-Test-TCS-021622
//
//  Created by Ajay on 2/17/22.
//

import Foundation
import SwiftUI

struct LandingDetailView: View {
    @Binding var isNavigationBarHidden: Bool
    @Binding var message: String
    @Binding var isPushed: Bool
    @Binding var user: User
    var body: some View {
        VStack{
            Spacer()
                .frame(height: 15)
            HStack {
                Spacer()
                Button("Logout") {
                    self.isPushed = false
                }
                .frame(width: 70, height: 30)
                .foregroundColor(.white)
                .background(Color.gray)
                .cornerRadius(5)
                .padding()
            }
            Spacer()
            
            VStack {
                Text("Welcome \(user.userName)")
            }
            
            Spacer()
        }
        .background().ignoresSafeArea()
        .navigationBarHidden(self.isNavigationBarHidden)
    }
    
}

struct LandingDetailView_preview: PreviewProvider {
    static var previews: some View {
        LandingDetailView(isNavigationBarHidden: .constant(true), message: .constant("Test"), isPushed: .constant(true), user: .constant(User(userName: "John", address: "NJ", role: "Developer")))
    }
}
